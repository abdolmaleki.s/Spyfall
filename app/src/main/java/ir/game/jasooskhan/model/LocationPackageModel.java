package ir.game.jasooskhan.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class LocationPackageModel extends RealmObject {

    public LocationPackageModel() {
        locations = new RealmList<>();
    }

    @PrimaryKey
    private long id;
    private String title;
    private boolean isSelected;
    private boolean isEditable;
    private int maxLocationCount;
    private String sku;
    private String price;
    private int difficulty;
    private String imageUrlMain;
    private String imageUrlDetailOne;
    private boolean isSaleable;
    private boolean isPurchased;
    private String imageUrlDetailTwo;
    private String imageUrlDetailThree;
    private String imageUrlDetailFour;
    public RealmList<LocationModel> locations;

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

    public void setMaxLocationCount(int maxLocationCount) {
        this.maxLocationCount = maxLocationCount;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public void setImageUrlMain(String imageUrlMain) {
        this.imageUrlMain = imageUrlMain;
    }

    public void setImageUrlDetailOne(String imageUrlDetailOne) {
        this.imageUrlDetailOne = imageUrlDetailOne;
    }

    public void setSaleable(boolean saleable) {
        isSaleable = saleable;
    }

    public void setPurchased(boolean purchased) {
        isPurchased = purchased;
    }

    public void setImageUrlDetailTwo(String imageUrlDetailTwo) {
        this.imageUrlDetailTwo = imageUrlDetailTwo;
    }

    public void setImageUrlDetailThree(String imageUrlDetailThree) {
        this.imageUrlDetailThree = imageUrlDetailThree;
    }

    public void setImageUrlDetailFour(String imageUrlDetailFour) {
        this.imageUrlDetailFour = imageUrlDetailFour;
    }

    public void setLocations(RealmList<LocationModel> locations) {
        this.locations = locations;
    }

    public int getMaxLocationCount() {
        return maxLocationCount;
    }

    public String getSku() {
        return sku;
    }

    public String getPrice() {
        return price;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public String getImageUrlMain() {
        return imageUrlMain;
    }

    public String getImageUrlDetailOne() {
        return imageUrlDetailOne;
    }

    public boolean isSaleable() {
        return isSaleable;
    }

    public boolean isPurchased() {
        return isPurchased;
    }

    public String getImageUrlDetailTwo() {
        return imageUrlDetailTwo;
    }

    public RealmList<LocationModel> getLocations() {
        return locations;
    }
}
