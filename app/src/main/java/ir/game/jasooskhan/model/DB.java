package ir.game.jasooskhan.model;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class DB {
    private static Realm realm;

    public static void init() {
        if (realm == null) {
            realm = Realm.getDefaultInstance();
        }
    }

    public static class LocationPackage {
        public static long insert(final LocationPackageModel packageModel) {
            try {

                realm.executeTransaction(new Realm.Transaction() { // must be in transaction for this to work
                    @Override
                    public void execute(Realm realm) {
                        // increment index
                        Number currentIdNum = realm.where(LocationPackageModel.class).max("id");
                        long nextId;
                        if (currentIdNum == null) {
                            nextId = 1001;
                        } else {
                            nextId = currentIdNum.longValue() + 1;
                        }
                        packageModel.setId(nextId);
                        realm.insertOrUpdate(packageModel);
                    }
                });
                return packageModel.getId();
            } catch (Exception e) {
                return -1;
            }
        }


        public static void clear() {

            final RealmResults<LocationPackageModel> results = realm.where(LocationPackageModel.class).findAll();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    results.deleteAllFromRealm();
                }
            });
        }

        public static boolean update(LocationPackageModel locationPackageModel, long id) {

            try {
                LocationPackageModel toEdit = realm.where(LocationPackageModel.class)
                        .equalTo("id", id).findFirst();
                realm.beginTransaction();
                realm.commitTransaction();
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        public static boolean addLocation(long packageId, LocationModel locationModel) {

            try {
                LocationPackageModel toEdit = realm.where(LocationPackageModel.class)
                        .equalTo("id", packageId).findFirst();
                realm.beginTransaction();
                toEdit.locations.add(locationModel);
                realm.commitTransaction();
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        public static boolean setSelected(long id, boolean isSelected) {

            try {
                LocationPackageModel toEdit = realm.where(LocationPackageModel.class)
                        .equalTo("id", id).findFirst();
                realm.beginTransaction();
                if (toEdit != null) {
                    toEdit.setSelected(isSelected);
                }
                realm.commitTransaction();
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        public static boolean setPurchased(long id, boolean isPurchased) {

            try {
                LocationPackageModel toEdit = realm.where(LocationPackageModel.class)
                        .equalTo("id", id).findFirst();
                realm.beginTransaction();
                if (toEdit != null) {
                    toEdit.setPurchased(isPurchased);
                }
                realm.commitTransaction();
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        public static RealmResults<LocationPackageModel> getAll() {
            return realm.where(LocationPackageModel.class).findAll();
        }

        public static RealmResults<LocationPackageModel> getSelectedPackages() {
            return realm.where(LocationPackageModel.class).equalTo("isSelected", true).findAll();
        }

        public static RealmResults<LocationPackageModel> getSaleablePackages() {
            return realm.where(LocationPackageModel.class).equalTo("isSaleable", true).and().equalTo("isPurchased", false).findAll();
        }

        public static RealmResults<LocationPackageModel> getUsablePackages() {
            return realm.where(LocationPackageModel.class).equalTo("isSaleable", false).or().equalTo("isPurchased", true).findAll();
        }

        public static LocationModel getLocationByPackageID(long id) {
            return realm.where(LocationModel.class).equalTo("id", id).findFirst();
        }

        public static LocationPackageModel getPackageBySKU(String sku) {
            return realm.where(LocationPackageModel.class).equalTo("sku", sku).findFirst();
        }

        public static boolean haveData() {
            RealmResults<LocationPackageModel> list = realm.where(LocationPackageModel.class).findAll();
            if (list.size() > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static class Player {
        public static long insert(final PlayerModel playerModel) {
            try {

                realm.executeTransaction(new Realm.Transaction() { // must be in transaction for this to work
                    @Override
                    public void execute(Realm realm) {
                        // increment index
                        Number currentIdNum = realm.where(PlayerModel.class).max("id");
                        long nextId;
                        if (currentIdNum == null) {
                            nextId = 1001;
                        } else {
                            nextId = currentIdNum.longValue() + 1;
                        }

                        Number currentPosition = realm.where(PlayerModel.class).max("position");
                        int position;

                        if (currentPosition == null) {
                            position = 0;
                        } else {
                            position = currentPosition.intValue() + 1;
                        }

                        playerModel.setId(nextId);
                        playerModel.setPosition(position);
                        realm.insertOrUpdate(playerModel);
                        realm.commitTransaction();
                    }
                });
                return playerModel.getId();
            } catch (Exception e) {
                return -1;
            }
        }

        public static boolean update(PlayerModel playerModel, long id) {

            try {
                PlayerModel toEdit = realm.where(PlayerModel.class)
                        .equalTo("id", id).findFirst();
                realm.beginTransaction();
                toEdit.setName(playerModel.getName());
                realm.commitTransaction();
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        public static boolean changePotition(long id, int newPosition) {
            try {
                PlayerModel toEdit = realm.where(PlayerModel.class)
                        .equalTo("id", id).findFirst();
                realm.beginTransaction();
                toEdit.setPosition(newPosition);
                realm.commitTransaction();
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        public static void clear() {

            final RealmResults<PlayerModel> results = realm.where(PlayerModel.class).findAll();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    results.deleteAllFromRealm();
                }
            });
        }

        public static void delete(long id) {

            final PlayerModel playerModel = realm.where(PlayerModel.class).equalTo("id", id).findFirst();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (playerModel != null) {
                        playerModel.deleteFromRealm();
                    }
                }
            });
        }

        public static RealmResults<PlayerModel> getAll() {
            //  return realm.where(PlayerModel.class).findAll();
            return realm.where(PlayerModel.class)
                    .sort("position", Sort.ASCENDING)
                    .findAll();
        }

        public static PlayerModel getPlayerByID(long id) {
            return realm.where(PlayerModel.class).equalTo("id", id).findFirst();

        }

    }

    public static class Location
    {

        public static void delete(String id) {

            final LocationModel locationModel = realm.where(LocationModel.class).equalTo("id", id).findFirst();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (locationModel != null) {
                        locationModel.deleteFromRealm();
                    }
                }
            });
        }
    }
}
