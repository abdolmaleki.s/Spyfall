package ir.game.jasooskhan.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PlayerModel extends RealmObject {

    @PrimaryKey
    long id;
    String name;
    int position;

    public PlayerModel(String name) {
        this.name = name;
    }

    public PlayerModel() {

    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
