package ir.game.jasooskhan.model;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class LocationModel extends RealmObject {

    @PrimaryKey
    String id;
    String name;

    public LocationModel(String name) {
        this.name = name;
        id = UUID.randomUUID().toString();
    }

    public LocationModel() {

    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

}
