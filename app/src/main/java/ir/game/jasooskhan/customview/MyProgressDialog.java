package ir.game.jasooskhan.customview;

import android.content.Context;
import android.os.Bundle;
import android.widget.ProgressBar;
import com.github.ybq.android.spinkit.style.MultiplePulse;
import ir.game.jasooskhan.R;

public class MyProgressDialog extends android.app.ProgressDialog {

    private ProgressBar progressBar;

    public MyProgressDialog(final Context context) {
        super(context, R.style.D1NoTitleDim);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_progress);
        progressBar = findViewById(R.id.dialog_progress_loading_progress);
        MultiplePulse multiplePulse = new MultiplePulse();
        progressBar.setIndeterminateDrawable(multiplePulse);
        setCancelable(false);

    }


}