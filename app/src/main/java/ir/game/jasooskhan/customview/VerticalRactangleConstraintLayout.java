package ir.game.jasooskhan.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class VerticalRactangleConstraintLayout extends FrameLayout {
    public VerticalRactangleConstraintLayout(Context context) {
        super(context);
    }

    public VerticalRactangleConstraintLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VerticalRactangleConstraintLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec + 24);
    }
}
