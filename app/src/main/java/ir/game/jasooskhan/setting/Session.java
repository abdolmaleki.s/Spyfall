package ir.game.jasooskhan.setting;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import ir.game.jasooskhan.application.Constant;

public class Session {
    private static SharedPreferences prefs;
    private static Session session;

    public static Session getInstance(Context cntx) {
        if (prefs == null) {
            prefs = PreferenceManager.getDefaultSharedPreferences(cntx.getApplicationContext());
        }
        if (session == null) {
            session = new Session();
        }

        return session;
    }

    public void setIsAGameFinished(boolean isAGameFinished) {
        prefs.edit().putBoolean(Constant.Key.IS_A_GAME_FINISHED, isAGameFinished).apply();
    }

    public boolean isAGameFinished() {

        boolean isaGameFinish = prefs.getBoolean(Constant.Key.IS_A_GAME_FINISHED, false);
        return isaGameFinish;
    }

    public void setIsGameRunning(boolean isGameRunning) {
        prefs.edit().putBoolean(Constant.Key.IS_GAME_RUNNING, isGameRunning).apply();
    }

    public boolean isGameRunning() {

        boolean isGameRunning = prefs.getBoolean(Constant.Key.IS_GAME_RUNNING, false);
        return isGameRunning;
    }


    public void setGameDuration(int duration) {
        prefs.edit().putInt(Constant.Key.GAME_DURATION, duration).apply();
    }

    public void setTimeIndex(int index) {
        prefs.edit().putInt(Constant.Key.TIME_INDEX, index).apply();
    }

    public void setSpySchool(boolean spySchool) {
        prefs.edit().putBoolean(Constant.Key.IS_SPY_SCHOOL_ACTIVE, spySchool).apply();
    }

    public void setMusicPlay(boolean isPlay) {
        prefs.edit().putBoolean(Constant.Key.IS_PLAY_MUSIC, isPlay).apply();
    }

    public void setSpyCount(int count) {
        prefs.edit().putInt(Constant.Key.SPY_COUNT, count).apply();
    }

    public int getGameDuration() {
        int gamaeDuration = prefs.getInt(Constant.Key.GAME_DURATION, 3);
        return gamaeDuration;
    }

    public int getTimeIndex() {
        int timeIndex = prefs.getInt(Constant.Key.TIME_INDEX, 0);
        return timeIndex;
    }

    public int getSpyCount() {
        int count = prefs.getInt(Constant.Key.SPY_COUNT, 1);
        return count;
    }

    public boolean isSpySchoolActive() {
        boolean isActive = prefs.getBoolean(Constant.Key.IS_SPY_SCHOOL_ACTIVE, false);
        return isActive;
    }

    public boolean isPlayMusic() {
        boolean isPlayMusic = prefs.getBoolean(Constant.Key.IS_PLAY_MUSIC, true);
        return isPlayMusic;
    }


}
