package ir.game.jasooskhan.mapper;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ir.game.jasooskhan.model.PlayerModel;
import ir.game.jasooskhan.viewmodel.PlayerViewModel;

public class PlayerMapper {

    public static List<PlayerViewModel> convertModelToViewmodel(List<PlayerModel> models) {
        List<PlayerViewModel> viewModels = new ArrayList<>();
        for (PlayerModel item : models) {
            PlayerViewModel viewModel = new PlayerViewModel();
            viewModel.id = item.getId();
            viewModel.name = item.getName();
            viewModels.add(viewModel);
        }
        return viewModels;
    }

    public static PlayerViewModel convertModelToViewmodel(PlayerModel model) {
        PlayerViewModel viewModel = new PlayerViewModel();
        viewModel.id = model.getId();
        viewModel.name = model.getName();
        return viewModel;
    }
}
