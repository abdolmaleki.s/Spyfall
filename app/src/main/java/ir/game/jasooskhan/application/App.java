package ir.game.jasooskhan.application;

import android.app.Application;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import ir.game.jasooskhan.R;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        configDB();
        configFont();
    }

    private void configDB() {
        try {
            Realm.init(this);

//            RealmConfiguration config = new RealmConfiguration.Builder()
//                    .name("jskhan.realm")
//                    .build();

            RealmConfiguration config = new RealmConfiguration.Builder()
                    .assetFile("jskhan.realm")
                    .build();

            Realm.setDefaultConfiguration(config);
        } catch (Exception e) {
        }
    }

    public void configFont() {

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/irsans.otf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());
    }


}
