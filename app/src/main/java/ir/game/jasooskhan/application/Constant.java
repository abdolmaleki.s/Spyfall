package ir.game.jasooskhan.application;

public class Constant {

    public static final String FIREBASE_TOPIC_GLOBAL = "SpyfallGlobal";
    public static final String PACKAGE_NAME = "ir.game.jasooskhan";
    public static final String BAZAR_KEY = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwCxHdK6Ou8Rin97GdK2+xsLFkAOTKHJwikCg1yCLBNHsMW00oIlba45QfGP0t4Mi4WcXneiuJ+ihdocdAz2/uP0cLumFZSV7xZlR2uymZtrUAoV9VB44stTfol1EweIR1oYhEBby3+s6SDLeM0kbCT4Vk8NP+RHrV4Mvj2B+pMxa3e1YauMIo3AyEhSAzLlcJ5ofbhbVRMGJlszxCQtxVQmitvefcj5kfmRS8t+79sCAwEAAQ==";


    public static class Key {

        public static final String TIME_REMAIN = "time.remain";
        public static final String IS_A_GAME_FINISHED = "is.game.finished";
        public static final String IS_SPY_SCHOOL_ACTIVE = "IS_SPY_SCHOOL_ACTIVE";
        public static final String IS_PLAY_MUSIC = "IS_PLAY_MUSIC";
        public static final String GAME_DURATION = "GAME_DURATION";
        public static final String TIME_INDEX = "TIME_INDEX";
        public static final String SPY_COUNT = "SPY_COUNT";

        public static final String IS_TUTORIAL_ADD_PLAYER = "IS_TUTORIAL_ADD_PLAYER";
        public static final String IS_TUTORIAL_DELETE_PLAYER = "IS_TUTORIAL_DELETE_PLAYER";
        public static final String IS_TUTORIAL_END_GAME = "IS_TUTORIAL_END_GAME";
        public static final String IS_TUTORIAL_END_GAME2 = "IS_TUTORIAL_END_GAME2";
        public static final String IS_TUTORIAL_END_GAME3 = "IS_TUTORIAL_END_GAME3";

        public static final String IS_TUTORIAL_DRAG_PLAYER = "IS_TUTORIAL_DRAG_PLAYER";

        public static final String SERVICE_COMMAND = "SERVICE_COMMAND";
        public static final String MUSIC_COMMAND = "MUSIC_COMMAND";
        public static final String IS_GAME_RUNNING = "IS_GAME_RUNNING";
    }
}
