package ir.game.jasooskhan.activity;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import ir.game.jasooskhan.application.Constant;
import ir.game.jasooskhan.service.MusicService;
import ir.game.jasooskhan.setting.Session;

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onStop() {
        super.onStop();
//        Intent intent = new Intent(this, MusicService.class);
//        intent.putExtra(Constant.Key.MUSIC_COMMAND, MusicService.STOP_MUSIC);
//        startService(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Session.getInstance(this).isPlayMusic()) {
            if (this instanceof TimerActivity || this instanceof HelpActivity) {
                Intent intent = new Intent(this, MusicService.class);
                intent.putExtra(Constant.Key.MUSIC_COMMAND, MusicService.STOP_MUSIC);
                startService(intent);
            } else {
                Intent intent = new Intent(this, MusicService.class);
                intent.putExtra(Constant.Key.MUSIC_COMMAND, MusicService.PLAY_MUSIC);
                startService(intent);
            }
        }
    }
}
