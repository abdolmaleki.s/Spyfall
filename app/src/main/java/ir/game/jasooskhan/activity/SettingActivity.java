package ir.game.jasooskhan.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import github.nisrulz.recyclerviewhelper.RVHItemClickListener;
import github.nisrulz.recyclerviewhelper.RVHItemTouchHelperCallback;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.adapter.PlayerAdapter;
import ir.game.jasooskhan.adapter.SettingPackageAdapter;
import ir.game.jasooskhan.application.Constant;
import ir.game.jasooskhan.customview.CenterScrollListener;
import ir.game.jasooskhan.customview.ScrollZoomLayoutManager;
import ir.game.jasooskhan.fragment.AddPlayerDialogFragment;
import ir.game.jasooskhan.infrastructure.Helper;
import ir.game.jasooskhan.mapper.PlayerMapper;
import ir.game.jasooskhan.model.DB;
import ir.game.jasooskhan.model.LocationPackageModel;
import ir.game.jasooskhan.model.PlayerModel;
import ir.game.jasooskhan.setting.Session;
import ir.game.jasooskhan.viewmodel.PlayerViewModel;
import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.FocusShape;
import me.toptas.fancyshowcase.listener.DismissListener;
import me.toptas.fancyshowcase.listener.OnViewInflateListener;

public class SettingActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView mRC_players;
    private RecyclerView mRC_packages;
    private Button mBTN_addPalyer;
    private Button mBTN_startGame;
    private Button mBTN_clearAll;
    private ImageView mBTN_selectPackage;
    private PlayerAdapter mPlayerAdapter;
    private SettingPackageAdapter mPackageAdapter;
    private Spinner mSP_spyCount;
    private Spinner mSP_gameDuration;
    private Session mSession;
    private FrameLayout mPanelSpycountCheck;
    private ImageView mIV_spyCountCheck;
    private int mGameDuration = 3;
    private TextView mTV_not_found_package;
    private boolean mISSpySchoolActive;
    private static final String TYPE_ADD_NEW_PLAYER = "TYPE.ADD.NEW.PLAYER";
    private static final String TYPE_EDIT_PLAYER_NAME = "TYPE.EDIT.PLAYER.NAME";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        Helper.animateTransition(this);
        setContentView(R.layout.activity_setting);
        initView();
        initPlayerAdapter();
        initPackagesAdapter();
        initSpyCounter();
        initGameDuration();
        addPlayerListListeners();
        checkIsNeedTutorial();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBTN_startGame.setEnabled(true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        List<PlayerViewModel> players = mPlayerAdapter.getDataset();
        if (mPlayerAdapter != null && players.size() > 1) {
            for (int i = 0; i < players.size(); i++) {
                DB.Player.changePotition(players.get(i).id, i);
            }
        }
    }

    @Override
    public void onClick(View view) {
        Helper.playClickSound(this);
        int id = view.getId();
        if (id == mBTN_addPalyer.getId()) {
            showAddPLayerDialog(TYPE_ADD_NEW_PLAYER, null);
        } else if (id == mBTN_startGame.getId()) {
            if (checkReadyFroStart()) {
                readyParamsAndGoToPlay();
            }
        } else if (id == mBTN_selectPackage.getId()) {
            goToPackageActivity();
        } else if (id == mBTN_clearAll.getId()) {
            clearAllPlayer();
        } else if (id == mPanelSpycountCheck.getId()) {
            configSpySchool();
        } else if (id == R.id.activity_setting_tv_spy_count_label) {
            mSP_spyCount.performClick();
        } else if (id == R.id.activity_setting_tv_game_duration_label) {
            mSP_gameDuration.performClick();
        }
    }

    private void configSpySchool() {
        if (mISSpySchoolActive) {
            mIV_spyCountCheck.setVisibility(View.GONE);
            mSession.setSpySchool(false);
        } else {
            mIV_spyCountCheck.setVisibility(View.VISIBLE);
            mSession.setSpySchool(true);
        }

        mISSpySchoolActive = !mISSpySchoolActive;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void initView() {
        mRC_players = findViewById(R.id.activity_setting_list_players);
        mRC_packages = findViewById(R.id.activity_setting_list_packages);
        mBTN_addPalyer = findViewById(R.id.activity_setting_btn_add_player);
        mBTN_selectPackage = findViewById(R.id.activity_setting_btn_select_package);
        mBTN_startGame = findViewById(R.id.activity_setting_btn_start_game);
        mBTN_clearAll = findViewById(R.id.activity_setting_btn_clear_all_players);
        mSP_spyCount = findViewById(R.id.activity_setting_sp_spy_count);
        mSP_gameDuration = findViewById(R.id.activity_setting_sp_game_duration);
        mPanelSpycountCheck = findViewById(R.id.activity_setting_panel_spy_check);
        mIV_spyCountCheck = findViewById(R.id.activity_setting_iv_spy_count_check);
        mTV_not_found_package = findViewById(R.id.activity_setting_not_package_found);
        findViewById(R.id.activity_setting_tv_spy_count_label).setOnClickListener(this);
        findViewById(R.id.activity_setting_tv_game_duration_label).setOnClickListener(this);
        mBTN_addPalyer.setOnClickListener(this);
        mBTN_startGame.setOnClickListener(this);
        mBTN_selectPackage.setOnClickListener(this);
        mBTN_clearAll.setOnClickListener(this);
        mPanelSpycountCheck.setOnClickListener(this);
        mSession = Session.getInstance(this);

        if (mSession.isSpySchoolActive()) {
            mIV_spyCountCheck.setVisibility(View.VISIBLE);
        }
    }

    public void initSpyCounter() {
        int playerCount = mPlayerAdapter.getItemCount();
        int maxSpy = (playerCount > 0) ? playerCount : 1;
        List<String> counts = new ArrayList<>();
        for (int i = 1; i <= maxSpy; i++) {
            counts.add(String.valueOf(i));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.item_spinner, counts);
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_drop_down);
        mSP_spyCount.setAdapter(dataAdapter);
        mSP_spyCount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String selectedItem = adapterView.getItemAtPosition(position).toString();
                mSession.setSpyCount(Integer.parseInt(selectedItem));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (mSP_spyCount.getCount() > 0) {
            mSP_spyCount.setSelection(0);
        }
    }

    private void initGameDuration() {
        List<String> durations = new ArrayList<>();
        for (int i = mGameDuration; i <= 15; i = i + 3) {
            durations.add(String.valueOf(i) + " دقیقه");
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.item_spinner, durations);
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_drop_down);
        mSP_gameDuration.setAdapter(dataAdapter);
        mSP_gameDuration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String selectedItem = adapterView.getItemAtPosition(position).toString();
                mGameDuration = Integer.parseInt(selectedItem.split(" ")[0]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (durations.size() > 0) {
            int timeIndex = mSession.getTimeIndex();
            mSP_gameDuration.setSelection(timeIndex);
        }
    }

    private void initPlayerAdapter() {

        List<PlayerModel> players = DB.Player.getAll();
        List<PlayerViewModel> viewModels = PlayerMapper.convertModelToViewmodel(players);
        mPlayerAdapter = new PlayerAdapter(viewModels, this, new PlayerAdapter.OnClickListener() {
            @Override
            public void onEdit(PlayerViewModel model) {
                PlayerModel playerModel = DB.Player.getPlayerByID(model.id);
                showAddPLayerDialog(TYPE_EDIT_PLAYER_NAME, playerModel);
            }
        });
        LinearLayoutManager llm = new LinearLayoutManager(this);
        mRC_players.setLayoutManager(llm);
        mRC_players.setAdapter(mPlayerAdapter);
    }

    private void initPackagesAdapter() {

        List<LocationPackageModel> packageModels = DB.LocationPackage.getSelectedPackages();
        if (packageModels.size() > 0) {
            mTV_not_found_package.setVisibility(View.GONE);
            mRC_packages.setVisibility(View.VISIBLE);
            mPackageAdapter = new SettingPackageAdapter(packageModels);
            ScrollZoomLayoutManager llm = new ScrollZoomLayoutManager(this, 0);
            mRC_packages.setLayoutManager(llm);
            mRC_packages.setAdapter(mPackageAdapter);
            mRC_packages.addOnScrollListener(new CenterScrollListener());
            mRC_packages.scrollToPosition(packageModels.size() / 2);
        } else {
            mTV_not_found_package.setVisibility(View.VISIBLE);
            mRC_packages.setVisibility(View.GONE);
        }
    }

    private void clearAllPlayer() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("آیا از حذف همه بازیکنان اطمینان دارید؟؟")
                .setCancelable(true)
                .setPositiveButton("بله", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DB.Player.clear();
                        initPlayerAdapter();
                    }
                }).setNegativeButton("خیر", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void goToPackageActivity() {
        startActivity(new Intent(this, PackageActivity.class));
        finish();
    }

    private boolean checkReadyFroStart() {
        List<LocationPackageModel> packageModels = DB.LocationPackage.getSelectedPackages();
        if (packageModels.size() < 1) {
            Helper.alert(this, "شما هیچ بسته ای از مکان ها را انتخاب نکرده اید");
            return false;
        }

        List<PlayerModel> playerModels = DB.Player.getAll();
        if (playerModels.size() < 3) {
            Helper.alert(this, "حداقل تعداد بازیکن برای شروع بازی 3 نفر است");
            return false;
        }

        int spyCount = Integer.parseInt(mSP_spyCount.getSelectedItem().toString());

        if (spyCount > playerModels.size()) {
            Helper.alert(this, "تعداد جاسوس ها نباید از تعداد بازیکنان بیشتر باشد");
            return false;
        }

        return true;
    }

    private void readyParamsAndGoToPlay() {
        mBTN_startGame.setEnabled(false);
        mSession.setGameDuration(mGameDuration);
        mSession.setTimeIndex(mSP_gameDuration.getSelectedItemPosition());
        mSession.setSpySchool(mISSpySchoolActive);
        onStop();
        Intent intent = new Intent(this, PlayActivity.class);
        startActivity(intent);
        finish();
    }

    private void showAddPLayerDialog(final String type, final PlayerModel currentPlayer) {
        final AddPlayerDialogFragment playerDialogFragment = new AddPlayerDialogFragment();
        if (type.equals(TYPE_EDIT_PLAYER_NAME)) {
            playerDialogFragment.show(this, currentPlayer.getName());

        } else {
            playerDialogFragment.show(this, null);

        }
        playerDialogFragment.setOnAddPlayerListener(new AddPlayerDialogFragment.OnAddPlayer() {
            @Override
            public void addPlayer(String name) {
                if (type.equals(TYPE_ADD_NEW_PLAYER)) {
                    PlayerModel playerModel = new PlayerModel(name);
                    DB.Player.insert(playerModel);
                    mPlayerAdapter.addPlayer(PlayerMapper.convertModelToViewmodel(playerModel));
                    initSpyCounter();
                    if (mPlayerAdapter.getItemCount() > 2) {
                        tutorialDragPlayer();
                    }
                } else {
                    PlayerModel playerModel = new PlayerModel(name);
                    DB.Player.update(playerModel, currentPlayer.getId());
                    initPlayerAdapter();
                    //mPlayerAdapter.notifyDataSetChanged();
                }
                //initAdapter();
                playerDialogFragment.dismiss();

            }
        });
    }

    private void addPlayerListListeners() {
        ItemTouchHelper.Callback callback = new RVHItemTouchHelperCallback(mPlayerAdapter, true, true, true);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(mRC_players);

        mRC_players.addOnItemTouchListener(new RVHItemClickListener(this, new RVHItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }
        }));
    }

    private void checkIsNeedTutorial() {
        tutorialDeletePlayer();
    }

    private void tutorialDeletePlayer() {

        new FancyShowCaseView.Builder(this)
                .focusOn(mRC_players)
                .showOnce(Constant.Key.IS_TUTORIAL_DELETE_PLAYER)
                .titleGravity(Gravity.BOTTOM)
                .customView(R.layout.custom_tutorial_delete_player, new OnViewInflateListener() {
                    @Override
                    public void onViewInflated(@NotNull View view) {

                    }
                })
                .dismissListener(new DismissListener() {
                    @Override
                    public void onDismiss(String id) {
                        tutorialAddPlayer();
                    }

                    @Override
                    public void onSkipped(String id) {
                        tutorialAddPlayer();
                    }
                })
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .roundRectRadius(90)
                .build()
                .show();

    }

    private void tutorialDragPlayer() {

        new FancyShowCaseView.Builder(this)
                .focusOn(mRC_players)
                .showOnce(Constant.Key.IS_TUTORIAL_DRAG_PLAYER)
                .titleGravity(Gravity.BOTTOM)
                .customView(R.layout.custom_tutorial_drag_player, new OnViewInflateListener() {
                    @Override
                    public void onViewInflated(@NotNull View view) {

                    }
                })
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .roundRectRadius(90)
                .build()
                .show();
    }

    private void tutorialAddPlayer() {

        new FancyShowCaseView.Builder(this)
                .focusOn(mBTN_addPalyer)
                .showOnce(Constant.Key.IS_TUTORIAL_ADD_PLAYER)
                .customView(R.layout.custom_tutorial_add_player, new OnViewInflateListener() {
                    @Override
                    public void onViewInflated(@NotNull View view) {

                    }
                })
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .roundRectRadius(90)
                .build()
                .show();
    }


}
