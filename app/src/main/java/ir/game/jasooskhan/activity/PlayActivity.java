package ir.game.jasooskhan.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.application.Constant;
import ir.game.jasooskhan.infrastructure.Helper;
import ir.game.jasooskhan.model.DB;
import ir.game.jasooskhan.model.LocationPackageModel;
import ir.game.jasooskhan.model.PlayerModel;
import ir.game.jasooskhan.service.TimerService;
import ir.game.jasooskhan.setting.Session;

public class PlayActivity extends BaseActivity implements View.OnClickListener {

    private String mSelectedLocation;
    private int mSpyCount;
    private List<Long> mSpyPlayerIndexes;
    private List<PlayerModel> mPlayerModels;
    private Button mBTN_submit;
    private TextView mTV_player_name;
    private TextView mTV_content_text;
    private int currentPlayerIndex = 0;
    private int mGameDuration;
    private boolean mIsSpySchoolActivation;
    private ImageView mIV_contentPanel;
    private FrameLayout mPanel_message;
    private ImageView mIV_shadow;
    private boolean isShowStatus;
    private MediaPlayer mMediaPlayer;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        Helper.animateTransition(this);
        setContentView(R.layout.activity_play);
        loadData();
        initView();
        initDB();
        makeGame();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBTN_submit.setEnabled(true);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        Helper.playClickSound(this);
        int id = view.getId();
        if (id == mBTN_submit.getId()) {

            if (currentPlayerIndex == mPlayerModels.size() - 1 && isShowStatus) {     // last player seen status
                mBTN_submit.setEnabled(false);
                Intent intent = new Intent(PlayActivity.this, TimerActivity.class);
                intent.putExtra("location", mSelectedLocation);
                startActivity(intent);
                finish();
            } else if (isShowStatus) { // player see status
                setCurrentPlayerState(++currentPlayerIndex);
            } else {
                showPlayerState();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void loadData() {
        mGameDuration = getIntent().getIntExtra(Constant.Key.GAME_DURATION, 5);
        mIsSpySchoolActivation = getIntent().getBooleanExtra(Constant.Key.IS_SPY_SCHOOL_ACTIVE, false);
    }

    private void initView() {
        mIsSpySchoolActivation = Session.getInstance(this).isSpySchoolActive();
        mBTN_submit = findViewById(R.id.activity_play_btn_submit);
        mTV_player_name = findViewById(R.id.activity_play_tv_player_name);
        mTV_content_text = findViewById(R.id.activity_play_tv_content_text);
        mPanel_message = findViewById(R.id.activity_play_panel_message);
        mIV_contentPanel = findViewById(R.id.activity_play_iv_content_panel);
        mIV_shadow = findViewById(R.id.activity_play_iv_shadow);
        mBTN_submit.setOnClickListener(this);

    }

    private void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void initDB() {
        DB.init();
        mPlayerModels = DB.Player.getAll();
    }

    private void makeGame() {
        mSpyCount = Session.getInstance(this).getSpyCount();
        selectLocation();
        setSpy();
        setCurrentPlayerState(currentPlayerIndex);
    }

    private void setCurrentPlayerState(int currentPlayerIndex) {
        mIV_shadow.setVisibility(View.GONE);
        mPanel_message.setVisibility(View.VISIBLE);
        mIV_contentPanel.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_play_content_location));
        mTV_player_name.setText("گوشی را به " + mPlayerModels.get(currentPlayerIndex).getName() + " بدید");
        mTV_content_text.setText("من " + mPlayerModels.get(currentPlayerIndex).getName() + " هستم");
        mBTN_submit.setText("تایید");
        isShowStatus = false;
    }

    private void setSpy() {

        mSpyPlayerIndexes = new ArrayList<>();

        if (mIsSpySchoolActivation) {// spy school on
            int randomSpySchoolChance = Helper.getRandomIndex(10);
            if (randomSpySchoolChance == 5) { //chance to activation spy school
                for (int i = 0; i < mPlayerModels.size(); i++) {
                    mSpyPlayerIndexes.add(Long.parseLong(i + ""));
                }

            } else {
                List<Integer> lotteryBasket = new ArrayList<>();
                for (int i = 0; i < mPlayerModels.size(); i++) {
                    lotteryBasket.add(i);
                }

                while (mSpyPlayerIndexes.size() < mSpyCount) {
                    int randomSpyIndex = Helper.getRandomIndex(lotteryBasket.size() - 1);
                    mSpyPlayerIndexes.add(Long.parseLong(lotteryBasket.get(randomSpyIndex) + ""));   // yadet bashe ke be jaye index db in dex list ro dari miriz intoo
                    lotteryBasket.remove(randomSpyIndex);
                }
            }
        } else {// spy school off
            List<Integer> lotteryBasket = new ArrayList<>();
            for (int i = 0; i < mPlayerModels.size(); i++) {
                lotteryBasket.add(i);
            }

            while (mSpyPlayerIndexes.size() < mSpyCount) {
                int randomSpyIndex = Helper.getRandomIndex(lotteryBasket.size() - 1);
                mSpyPlayerIndexes.add(Long.parseLong(lotteryBasket.get(randomSpyIndex) + ""));   // yadet bashe ke be jaye index db in dex list ro dari miriz intoo
                lotteryBasket.remove(randomSpyIndex);
            }
        }
    }

    private void showPlayerState() {
        boolean isSpy = mSpyPlayerIndexes.contains(Long.parseLong(currentPlayerIndex + ""));
        if (isSpy) {
            showSpyPanel();
        } else {
            showLocationPanel();
        }

        isShowStatus = true;
    }

    private void showLocationPanel() {
        mIV_contentPanel.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_play_content_location));
        mPanel_message.setVisibility(View.INVISIBLE);
        mBTN_submit.setText("فهمیدم");
        mIV_shadow.setVisibility(View.VISIBLE);
        mTV_content_text.setText("اسم رمز: " + mSelectedLocation);
    }

    private void showSpyPanel() {
        mIV_contentPanel.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_play_content_spy));
        mPanel_message.setVisibility(View.INVISIBLE);
        mBTN_submit.setText("فهمیدم");
        mIV_shadow.setVisibility(View.VISIBLE);
        mTV_content_text.setText("شما جاسوس هستید");
    }

    private void selectLocation() {
        List<LocationPackageModel> locationGroups = DB.LocationPackage.getSelectedPackages();
        int groupCounts = locationGroups.size();
        if (groupCounts == 1) {
            int locationCount = locationGroups.get(0).locations.size();
            if (locationCount == 1) {
                mSelectedLocation = locationGroups.get(0).locations.get(0).getName();

            } else {
                int randomLocationIndex = Helper.getRandomIndex(locationCount - 1);
                mSelectedLocation = locationGroups.get(0).locations.get(randomLocationIndex).getName();
            }
        } else {
            int randomGroupIndex = Helper.getRandomIndex(locationGroups.size() - 1);
            LocationPackageModel selectedGroup = locationGroups.get(randomGroupIndex);
            int randomLocationIndex = Helper.getRandomIndex(selectedGroup.locations.size() - 1);
            mSelectedLocation = selectedGroup.locations.get(randomLocationIndex).getName();
        }

    }

    @Override
    public void onBackPressed() {
        Helper.playClickSound(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("می خواهید از بازی خارج شوید؟")
                .setCancelable(true)
                .setPositiveButton("بله", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        stopService(new Intent(PlayActivity.this, TimerService.class));
                        Session.getInstance(getApplicationContext()).setIsAGameFinished(false);
                        goToMain();
                    }

                }).setNegativeButton("خیر", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        finish();
    }
}
