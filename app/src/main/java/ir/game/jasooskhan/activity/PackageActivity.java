package ir.game.jasooskhan.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;

import java.util.List;

import androidx.annotation.Nullable;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.adapter.PackagesAdapter;
import ir.game.jasooskhan.application.Constant;
import ir.game.jasooskhan.infrastructure.Helper;
import ir.game.jasooskhan.model.DB;
import ir.game.jasooskhan.model.LocationPackageModel;
import ir.game.jasooskhan.util.IabHelper;
import ir.game.jasooskhan.util.IabResult;
import ir.game.jasooskhan.util.Inventory;

public class PackageActivity extends BaseActivity {

    ExpandableListView mList_packages;
    private IabHelper mBazarHelper;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        Helper.animateTransition(this);
        setContentView(R.layout.activity_package);
        initView();
        initDB();
        connectToBazaar();
    }

    private void connectToBazaar() {
        if (Helper.isInternetConnected(this)) {
            Helper.progressBar.showDialog(this);
            mBazarHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                public void onIabSetupFinished(IabResult result) {
                    if (!result.isSuccess()) {
                        Helper.progressBar.hideDialog();
                        initAdapter();
                    } else {
                        ////////////////////////////////////////////////////////////////////////////////////
                        ///////////// Request for purchased Packages /////////////////////////////////////////////
                        ////////////////////////////////////////////////////////////////////////////////////
                        requestForPuchasedPackages();
                    }
                }
            });
        } else {
            initAdapter();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBazarHelper != null) mBazarHelper.dispose();
        mBazarHelper = null;
    }

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void initDB() {
        DB.init();
    }

    private void initAdapter() {
        List<LocationPackageModel> packageModels = DB.LocationPackage.getUsablePackages();
        PackagesAdapter packagesAdapter = new PackagesAdapter(this, packageModels);
        mList_packages.setAdapter(packagesAdapter);
    }

    private void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void initView() {
        mList_packages = findViewById(R.id.activity_package_list);
        mBazarHelper = new IabHelper(this, Constant.BAZAR_KEY);
    }

    public void goToMainActivity(View view) {
        Helper.playClickSound(this);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        finish();

    }

    public void goToSettingActivity(View view) {
        Helper.playClickSound(this);
        Intent intent = new Intent(this, SettingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        finish();
    }

    private void requestForPuchasedPackages() {
        IabHelper.QueryInventoryFinishedListener mGotInventoryListener
                = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result,
                                                 Inventory inventory) {

                Helper.progressBar.hideDialog();

                if (result.isFailure()) {

                } else {
                    List<LocationPackageModel> packageModels = DB.LocationPackage.getSaleablePackages();
                    for (LocationPackageModel item : packageModels) {
                        if (inventory.hasPurchase(item.getSku())) {
                            LocationPackageModel packageModel = DB.LocationPackage.getPackageBySKU(item.getSku());
                            DB.LocationPackage.setPurchased(packageModel.getId(), true);
                        }
                    }
                }
                initAdapter();
            }
        };

        mBazarHelper.queryInventoryAsync(mGotInventoryListener);
    }
}
