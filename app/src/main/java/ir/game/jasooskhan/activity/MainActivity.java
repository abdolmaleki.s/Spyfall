package ir.game.jasooskhan.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.farsitel.bazaar.IUpdateCheckService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import androidx.annotation.NonNull;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.application.Constant;
import ir.game.jasooskhan.infrastructure.Helper;
import ir.game.jasooskhan.model.DB;
import ir.game.jasooskhan.service.MusicService;
import ir.game.jasooskhan.setting.Session;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIV_music;
    private Session mSession;
    private IUpdateCheckService mUpdateservice;
    private UpdateServiceConnection mConnection;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        Helper.animateTransition(this);
        setContentView(R.layout.activity_main);
        initView();
        initDB();
        ConfigFirebase();
        initUpdateService();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mSession.isGameRunning()) {
            startActivity(new Intent(this, TimerActivity.class));
        }
        if (mSession.isPlayMusic()) {
            //runMusicBackground();
        } else {
            mIV_music.setImageResource(R.drawable.ic_volume_slash);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, MusicService.class));
        releaseUpdateService();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.activity_main_btn_start) {
            Helper.shakeView(findViewById(R.id.activity_main_btn_start));
            startActivityWithDelay(SettingActivity.class);
        } else if (id == R.id.activity_main_btn_tutorial) {
            Helper.shakeView(findViewById(R.id.activity_main_btn_tutorial));
            startActivityWithDelay(HelpActivity.class);
        } else if (id == R.id.activity_main_btn_locations) {
            Helper.shakeView(findViewById(R.id.activity_main_btn_locations));
            startActivityWithDelay(PackageActivity.class);
        } else if (id == R.id.activity_main_btn_about_us) {
            Helper.shakeView(findViewById(R.id.activity_main_btn_about_us));
            startActivityWithDelay(AboutActivity.class);
        } else if (id == R.id.activity_main_btn_shop) {
            Helper.shakeView(findViewById(R.id.activity_main_btn_shop));
            if (Helper.isInternetConnected(this)) {
                startActivityWithDelay(ShopActivity.class);
            } else {
                Helper.alert(this, "برای ورود به مغازه باید اینترنت وصل باشد");
            }
        } else if (id == mIV_music.getId()) {
            handelMusicVoloume();
        }
    }

    private void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void initDB() {
        DB.init();
    }

    private void initView() {
        mSession = Session.getInstance(this);
        mIV_music = findViewById(R.id.activity_main_btn_music);
        mIV_music.setOnClickListener(this);
        mHandler = new Handler();
        findViewById(R.id.activity_main_btn_start).setOnClickListener(this);
        findViewById(R.id.activity_main_btn_tutorial).setOnClickListener(this);
        findViewById(R.id.activity_main_btn_locations).setOnClickListener(this);
        findViewById(R.id.activity_main_btn_about_us).setOnClickListener(this);
        findViewById(R.id.activity_main_btn_secret).setOnClickListener(this);
        findViewById(R.id.activity_main_btn_shop).setOnClickListener(this);
    }

    private void handelMusicVoloume() {
        boolean isPlayMusic = mSession.isPlayMusic();
        if (isPlayMusic) {
            mIV_music.setImageResource(R.drawable.ic_volume_slash);
            stopService(new Intent(this, MusicService.class));
        } else {
            mIV_music.setImageResource(R.drawable.ic_volume_up);
            runMusicBackground();
        }
        mSession.setMusicPlay(!isPlayMusic);
    }

    private void runMusicBackground() {
        Intent intent = new Intent(this, MusicService.class);
        intent.putExtra(Constant.Key.MUSIC_COMMAND, MusicService.PLAY_MUSIC);
        startService(intent);
    }

    private void ConfigFirebase() {
        FirebaseMessaging.getInstance().subscribeToTopic(Constant.FIREBASE_TOPIC_GLOBAL)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
    }

    private void initUpdateService() {
        mConnection = new UpdateServiceConnection();
        Intent i = new Intent(
                "com.farsitel.bazaar.service.UpdateCheckService.BIND");
        i.setPackage("com.farsitel.bazaar");
        boolean ret = bindService(i, mConnection, Context.BIND_AUTO_CREATE);
    }

    private void releaseUpdateService() {
        unbindService(mConnection);
        mConnection = null;
    }

    class UpdateServiceConnection implements ServiceConnection {
        public void onServiceConnected(ComponentName name, IBinder boundService) {
            mUpdateservice = IUpdateCheckService.Stub
                    .asInterface((IBinder) boundService);
            try {
                long vCode = mUpdateservice.getVersionCode(Constant.PACKAGE_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onServiceDisconnected(ComponentName name) {
            mUpdateservice = null;
        }
    }

    private void startActivityWithDelay(Class activity) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(MainActivity.this, activity));
            }
        }, 450);


    }

}