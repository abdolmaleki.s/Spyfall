package ir.game.jasooskhan.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.adapter.ShopPackageAdapter;
import ir.game.jasooskhan.application.Constant;
import ir.game.jasooskhan.customview.CenterScrollListener;
import ir.game.jasooskhan.customview.ScrollZoomLayoutManager;
import ir.game.jasooskhan.fragment.SpecificShopDialogFragment;
import ir.game.jasooskhan.infrastructure.Helper;
import ir.game.jasooskhan.model.DB;
import ir.game.jasooskhan.model.LocationPackageModel;
import ir.game.jasooskhan.util.IabHelper;
import ir.game.jasooskhan.util.IabResult;
import ir.game.jasooskhan.util.Inventory;
import ir.game.jasooskhan.util.Purchase;

public class ShopActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView mList_Packages;
    private ShopPackageAdapter mPackageAdapter;
    private TextView mTV_not_found_package;
    private Handler mHandler;
    private ScrollZoomLayoutManager mLayoutManager;
    private LinearLayout mBTN_showPackage;
    private TextView mTV_price;
    static final int PURCHASE_REQUEST_CODE = 1023;
    private IabHelper mBazarHelper;
    private BazarPurchaseListener mBazarPurchaseListener;
    private List<LocationPackageModel> mPackageModels;
    private LocationPackageModel mCurrentPackage;
    private boolean isConnectedToBazaar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        Animatoo.animateFade(this);
        Helper.animateTransition(this);
        setContentView(R.layout.activity_shop);
        initDB();
        initView();
        getBazaarKey();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBazarHelper != null && isConnectedToBazaar) {
            mBazarHelper.dispose();
        }

        mBazarHelper = null;
        mHandler = null;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Pass on the activity result to the helper for handling
        if (!mBazarHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        } else {

        }
    }

    private void initDB() {
        DB.init();
    }

    private void initAdapter() {
        mPackageModels = DB.LocationPackage.getSaleablePackages();
        if (mPackageModels.size() > 0) {
            mCurrentPackage = mPackageModels.get(0);
            //mTV_price.setText(mCurrentPackage.getPrice() + " تومان");
            mTV_not_found_package.setVisibility(View.GONE);
            mList_Packages.setVisibility(View.VISIBLE);
            mPackageAdapter = new ShopPackageAdapter(mPackageModels, new ShopPackageAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(LocationPackageModel model) {
                    showPackageDetail(model);
                }
            });
            mLayoutManager = new ScrollZoomLayoutManager(this, 0);
            mList_Packages.setLayoutManager(mLayoutManager);
            mList_Packages.setAdapter(mPackageAdapter);
            mList_Packages.addOnScrollListener(new PackageListScrollListener());
        } else {
            mTV_not_found_package.setVisibility(View.VISIBLE);
            mList_Packages.setVisibility(View.GONE);
        }
    }

    private void initView() {
        mList_Packages = findViewById(R.id.activity_shop_list_packages);
        mBTN_showPackage = findViewById(R.id.activity_shop_btn_purchase);
        mTV_price = findViewById(R.id.activity_shop_tv_price);
        mTV_not_found_package = findViewById(R.id.activity_shop_tv_packages_not_found);
        mHandler = new Handler();
        mBTN_showPackage.setOnClickListener(this);
    }

    private void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void requestBuyPackage(String sku) {
        mBazarPurchaseListener = new BazarPurchaseListener();

        ////////////////////////////////////////////////////////////////////////////////////
        ///////////// Connecting to cafe bazar if needed /////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        if (!isConnectedToBazaar) {
            mBazarHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                public void onIabSetupFinished(IabResult result) {
                    if (!result.isSuccess()) {
                        Toast.makeText(ShopActivity.this, "خطا در اتصال به کافه بازار",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        ////////////////////////////////////////////////////////////////////////////////////
                        ///////////// Request for purchase /////////////////////////////////////////////
                        ////////////////////////////////////////////////////////////////////////////////////
                        mBazarHelper.launchPurchaseFlow(ShopActivity.this, sku, 10001,
                                mBazarPurchaseListener);
                    }
                }
            });
        } else {
            mBazarHelper.launchPurchaseFlow(ShopActivity.this, sku, 10001,
                    mBazarPurchaseListener);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == mBTN_showPackage.getId()) {
            showPackageDetail();
        }
    }

    private void showPackageDetail() {
        SpecificShopDialogFragment specificShopDialogFragment = new SpecificShopDialogFragment();
        specificShopDialogFragment.showSpecificShopPackage(ShopActivity.this, mCurrentPackage, new SpecificShopDialogFragment.OnPurchaseClickListener() {
            @Override
            public void onItemClick(LocationPackageModel model) {
                requestBuyPackage(model.getSku());
                specificShopDialogFragment.dismiss();
            }
        });
    }

    private void showPackageDetail(LocationPackageModel model) {
        SpecificShopDialogFragment specificShopDialogFragment = new SpecificShopDialogFragment();
        specificShopDialogFragment.showSpecificShopPackage(ShopActivity.this, model, new SpecificShopDialogFragment.OnPurchaseClickListener() {
            @Override
            public void onItemClick(LocationPackageModel model) {
                requestBuyPackage(model.getSku());
                specificShopDialogFragment.dismiss();
            }
        });
    }

    private void getBazaarKey() {
        connectToBazaar(Constant.BAZAR_KEY);
    }

    private void connectToBazaar(String bazarKey) {
        mBazarHelper = new IabHelper(this, bazarKey);
        if (Helper.isInternetConnected(this)) {
            Helper.progressBar.showDialog(this);
            try {
                mBazarHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                    public void onIabSetupFinished(IabResult result) {
                        if (!result.isSuccess()) {
                            Helper.progressBar.hideDialog();
                            initAdapter();
                        } else {
                            ////////////////////////////////////////////////////////////////////////////////////
                            ///////////// Request for purchased Packages /////////////////////////////////////////////
                            ////////////////////////////////////////////////////////////////////////////////////
                            isConnectedToBazaar = true;
                            requestForPuchasedPackages();
                        }
                    }
                });
            } catch (Exception e) {
                if (e instanceof SecurityException) {
                    Toast.makeText(this, "ابتدا برنامه کافه بازار را نصب کرده سپس جاسوس خان را نصب کنید", Toast.LENGTH_LONG).show();
                }
            }

        } else {
            initAdapter();
        }
    }

    private void requestForPuchasedPackages() {
        IabHelper.QueryInventoryFinishedListener mGotInventoryListener
                = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result,
                                                 Inventory inventory) {

                Helper.progressBar.hideDialog();

                if (result.isFailure()) {

                } else {
                    List<LocationPackageModel> packageModels = DB.LocationPackage.getSaleablePackages();
                    for (LocationPackageModel item : packageModels) {
                        if (inventory.hasPurchase(item.getSku())) {
                            LocationPackageModel packageModel = DB.LocationPackage.getPackageBySKU(item.getSku());
                            DB.LocationPackage.setPurchased(packageModel.getId(), true);
                        }
                    }
                }
                initAdapter();
            }
        };

        mBazarHelper.queryInventoryAsync(mGotInventoryListener);
    }

    private class BazarPurchaseListener implements IabHelper.OnIabPurchaseFinishedListener {

        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                Toast.makeText(ShopActivity.this, "عملیات خرید لغو شد", Toast.LENGTH_SHORT).show();
                return;
            } else {
                LocationPackageModel packageModel = DB.LocationPackage.getPackageBySKU(purchase.getSku());
                DB.LocationPackage.setPurchased(packageModel.getId(), true);
                initAdapter();
                Toast.makeText(ShopActivity.this, "عملیات خرید با موفقیت انجام شد", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class PackageListScrollListener extends CenterScrollListener {

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                mCurrentPackage = mPackageModels.get(mLayoutManager.getCurrentPosition());
                mTV_price.setText(mCurrentPackage.getPrice() + " تومان");
            }
        }
    }
}
