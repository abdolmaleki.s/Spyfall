package ir.game.jasooskhan.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import antonkozyriatskyi.circularprogressindicator.CircularProgressIndicator;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.adapter.LocationAdapter;
import ir.game.jasooskhan.application.Constant;
import ir.game.jasooskhan.infrastructure.Helper;
import ir.game.jasooskhan.model.DB;
import ir.game.jasooskhan.model.LocationModel;
import ir.game.jasooskhan.model.LocationPackageModel;
import ir.game.jasooskhan.service.MusicService;
import ir.game.jasooskhan.service.TimerService;
import ir.game.jasooskhan.setting.Session;
import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.FocusShape;
import me.toptas.fancyshowcase.listener.DismissListener;
import me.toptas.fancyshowcase.listener.OnViewInflateListener;

import static ir.game.jasooskhan.application.Constant.Key.TIME_REMAIN;

public class TimerActivity extends BaseActivity {

    private CircularProgressIndicator circularProgress;
    private long mGameDuration;
    private RecyclerView mRCV_locations;
    private Session mSession;
    private boolean mIsGamePaused;
    private Button mCancelButton;
    private Button mPauseButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        Helper.animateTransition(this);
        setContentView(R.layout.activity_timer);
        loadDate();
        initDB();
        initView();
        runService();
        makeScreenOn();
    }

    private void makeScreenOn() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void initDB() {
        DB.init();
    }

    private void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        stopMusic();
        registerReceiver(timeReceiver, new IntentFilter(TimerService.COUNTDOWN_BR));
        boolean isAGameFinished = Session.getInstance(getApplicationContext()).isAGameFinished();
        if (isAGameFinished) {
            circularProgress.setProgress(0, mGameDuration);
            mCancelButton.setText("خروج");
            mPauseButton.setText("بازی مجدد");
            stopService(new Intent(this, TimerService.class));
        }
    }

    private void stopMusic() {
        stopService(new Intent(this, MusicService.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(timeReceiver);

        } catch (Exception e) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, TimerService.class));
    }

    @Override
    public void onBackPressed() {
        Helper.playClickSound(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("می خواهید از بازی خارج شوید؟")
                .setCancelable(true)
                .setPositiveButton("بله", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Session.getInstance(getApplicationContext()).setIsAGameFinished(false);
                        goToMain();
                        finish();
                    }

                }).setNegativeButton("خیر", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void loadDate() {
        mSession = Session.getInstance(this);
        mGameDuration = 60 * mSession.getGameDuration() * 1000;
    }

    private void initView() {
        mRCV_locations = findViewById(R.id.activity_timer_grid_location);
        mCancelButton = findViewById(R.id.activity_timer_btn_cancel);
        mPauseButton = findViewById(R.id.activity_timer_btn_pause);
        initAdapter();
        initTimer();
        circularProgress = findViewById(R.id.activity_timer_circular_progress);
        circularProgress.setProgressStrokeCap(CircularProgressIndicator.CAP_BUTT);
        circularProgress.setProgressTextAdapter(new CircularProgressIndicator.ProgressTextAdapter() {
            @NonNull
            @Override
            public String formatText(double v) {
                long timeInMilisecond = (long) v;
                long m = (timeInMilisecond / 1000) / 60;
                long s = (timeInMilisecond / 1000) % 60;
                String minute = (m > 9) ? ("" + m) : ("0" + m);
                String second = (s > 9) ? ("" + s) : ("0" + s);

                return minute + ":" + second;
            }
        });

    }

    private void initAdapter() {
        List<LocationPackageModel> selectedPackages = DB.LocationPackage.getSelectedPackages();
        List<String> locationNames = new ArrayList<>();
        for (LocationPackageModel item : selectedPackages) {
            for (LocationModel location : item.locations) {
                locationNames.add(location.getName());
            }
        }

        LocationAdapter locationAdapter = new LocationAdapter(locationNames);
        mRCV_locations.setLayoutManager(new GridLayoutManager(this, 3));
        mRCV_locations.setAdapter(locationAdapter);
    }

    private void initTimer() {
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private BroadcastReceiver timeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateGUI(intent); // or whatever method used to update your GUI fields
        }
    };

    private void updateGUI(Intent intent) {
        if (intent.getExtras() != null) {
            long millisUntilFinished = intent.getLongExtra(TIME_REMAIN, 0);
            long m = (millisUntilFinished / 1000) / 60;
            long s = (millisUntilFinished / 1000) % 60;
            if (m == 0 && s == 0) {
                mCancelButton.setText("خروج");
                mPauseButton.setText("بازی مجدد");
                checkIsNeedTutorial();
                //stopService(new Intent(this, TimerService.class));
            }
            circularProgress.setProgress(millisUntilFinished, mGameDuration);
        }
    }

    private void checkIsNeedTutorial() {
        tutorialEndGame1();
    }

    public void cancelGame(View view) {
        Helper.playClickSound(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("می خواهید از بازی خارج شوید؟")
                .setCancelable(true)
                .setPositiveButton("بله", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        stopService(new Intent(TimerActivity.this, TimerService.class));
                        Session.getInstance(getApplicationContext()).setIsAGameFinished(false);
                        goToMain();
                    }
                }).setNegativeButton("خیر", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        AlertDialog alert = builder.create();
        alert.show();


    }

    private void goToMain() {
        stopService(new Intent(this, TimerService.class));
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void runService() {
        Session.getInstance(getApplicationContext()).setIsAGameFinished(false);
        Intent intent = new Intent(this, TimerService.class);
        intent.putExtra(Constant.Key.SERVICE_COMMAND, TimerService.COMMAND_RESUME);
        startService(intent);
    }

    public void pauseGame(View view) {
        boolean isAGameFinished = Session.getInstance(getApplicationContext()).isAGameFinished();
        if (isAGameFinished) {
            stopService(new Intent(this, TimerService.class));
            startActivity(new Intent(this, PlayActivity.class));
            finish();
        } else {
            if (mIsGamePaused) {
                Intent intent = new Intent(this, TimerService.class);
                intent.putExtra(Constant.Key.SERVICE_COMMAND, TimerService.COMMAND_RESUME);
                startService(intent);
                mPauseButton.setText("توقف بازی");
            } else {
                Intent intent = new Intent(this, TimerService.class);
                intent.putExtra(Constant.Key.SERVICE_COMMAND, TimerService.COMMAND_PAUSE);
                startService(intent);
                mPauseButton.setText("ادامه بازی");

            }

            mIsGamePaused = !mIsGamePaused;
        }
    }

    private void tutorialEndGame1() {

        new FancyShowCaseView.Builder(this)
                .showOnce(Constant.Key.IS_TUTORIAL_END_GAME)
                .titleGravity(Gravity.BOTTOM)
                .customView(R.layout.custom_tutorial_end_game_1, new OnViewInflateListener() {
                    @Override
                    public void onViewInflated(@NotNull View view) {

                    }
                })
                .dismissListener(new DismissListener() {
                    @Override
                    public void onDismiss(String id) {
                        tutorialEndGame2();
                    }

                    @Override
                    public void onSkipped(String id) {
                        tutorialEndGame2();
                    }
                })
                .build()
                .show();
    }

    private void tutorialEndGame2() {

        new FancyShowCaseView.Builder(this)
                .showOnce(Constant.Key.IS_TUTORIAL_END_GAME2)
                .titleGravity(Gravity.BOTTOM)
                .customView(R.layout.custom_tutorial_end_game_2, new OnViewInflateListener() {
                    @Override
                    public void onViewInflated(@NotNull View view) {

                    }
                })
                .dismissListener(new DismissListener() {
                    @Override
                    public void onDismiss(String id) {
                        tutorialEndGame3();
                    }

                    @Override
                    public void onSkipped(String id) {
                        tutorialEndGame3();
                    }
                })
                .build()
                .show();
    }

    private void tutorialEndGame3() {

        new FancyShowCaseView.Builder(this)
                .focusOn(mRCV_locations)
                .showOnce(Constant.Key.IS_TUTORIAL_END_GAME3)
                .titleGravity(Gravity.BOTTOM)
                .customView(R.layout.custom_tutorial_end_game_3, new OnViewInflateListener() {
                    @Override
                    public void onViewInflated(@NotNull View view) {

                    }
                })
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .roundRectRadius(90)
                .build()
                .show();
    }
}
