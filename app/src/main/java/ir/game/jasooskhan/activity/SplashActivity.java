package ir.game.jasooskhan.activity;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.VideoView;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.model.DB;

public class SplashActivity extends Activity {

    Thread splashTread;
    private VideoView mVideoView;
    private boolean isManualFinished;
    private boolean isPlayedFinalVideo = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        setContentView(R.layout.activity_splash);
        initView();
        initDB();
        startSplash();
    }

    @Override
    protected void onStop() {
        super.onStop();
        splashTread.interrupt();
        splashTread = null;
    }

    @Override
    public void onBackPressed() {

    }

    private void initView() {
        mVideoView = findViewById(R.id.activity_splash_video);
    }

    private void initDB() {
        DB.init();
    }

    private void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void startSplash() {
        playVideo();
        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 13000) {
                        sleep(100);
                        waited += 100;
                    }

                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    if (!isManualFinished) {
                        goToMain();
                    }
                }
            }
        };
        splashTread.start();
    }

    private void playVideo() {
        try {
            mVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.imagine));
            mVideoView.requestFocus();
            mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    mVideoView.start();
                }
            });
            mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    if (!isPlayedFinalVideo) {
                        mVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.splash));
                        mVideoView.requestFocus();
                        mVideoView.start();
                        isPlayedFinalVideo = true;
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        mVideoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isManualFinished = true;
                goToMain();
            }
        });

    }

    private void goToMain() {

        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}