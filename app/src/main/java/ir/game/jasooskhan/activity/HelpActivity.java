package ir.game.jasooskhan.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.io.IOException;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.customview.roundedvideo.VideoSurfaceView;
import ir.game.jasooskhan.infrastructure.Helper;
import ir.game.jasooskhan.service.MusicService;

public class HelpActivity extends BaseActivity {

    private VideoSurfaceView mVideoView;
    private TextView mTV_help;
    private MediaPlayer mMediaPlayer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        Helper.animateTransition(this);
        setContentView(R.layout.activity_help);
        initView();
        playVideo();
        stopMusic();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMediaPlayer.stop();
        mMediaPlayer.release();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void initView() {
        mVideoView = findViewById(R.id.activity_help_video);
        mTV_help = findViewById(R.id.activity_help_tv_help);
        setHtmlText(mTV_help, "<p><b>جاسوس خان چیست؟</b></p><br/>\n" +
                "\n" +
                "<p>شاید خیلی از شماها دیگه تا الان بازی مافیا و یا همون گرگ بازی را کرده باشید، جاسوس خان هم از همون دسته بازی هاست.</p>\n" +
                "\n" +
                "<p>هدف از این بازی اینه که افراد معمولی، جاسوس رو از بین خودشون پیدا کنند. جاسوس هم باید از مکان منتخب این افراد اطلاع پیدا کنه. جاسوس با سوالاتی که بین بازیکن ها رد و بدل میشه باید سعی کنه تا مکان مورد نظر رو حدس بزنه، بقیه بازیکن ها هم باید با سوال و جواب سعی کنند جاسوس رو از بین خودشون پیدا کنند ولی نباید سوالاتی بپرسند که مکان انتخابی رو پیش جاسوس تابلو کنند. حالا پایین تر با مثال بهتر متوجه میشید.</p><br/>\n" +
                "\n" +
                "<p><b>شروع بازی</b></p>\n" +
                "\n" +
                "<p>برای شروع، به منوی اصلی رفته و شروع بازی رو انتخاب کنید، در اونجا شما می بینید که چند تا کار باید انجام بدید:</p>\n" +
                "\n" +
                "<p>• اسم بازیکن هایی که قراره شرکت کنند رو دونه دونه وارد کنید.</p>\n" +
                "\n" +
                "<p>• مکان های منتخب خودتون رو انتخاب کنید.</p>\n" +
                "\n" +
                "<p>• زمان بازی رو مشخص کنید (پیشنهاد ما زمان 10 دقیقه برای تعداد 6 نفر کمتر است، اگر بیشتر هستید زمان رو هم بیشتر کنید.)</p>\n" +
                "\n" +
                "<p>• شما می تونید تعداد جاسوس ها رو هم انتخاب کنید، به جای یک نفر می تونید چند نفر رو بزارید، فقط نکته ای که وجود داره اینه که جاسوس ها هم تیمی نیستند.</p></b>\n" +
                "\n" +
                "<p>اگر شما گزینه \"همه جاسوس\" رو فعال کنید درصد کمی احتمال داره که تو بازی همه بازیکن ها جاسوس بشن.</p></b>\n" +
                "\n" +
                "<p>بازی رو شروع کنید.</p><br/>\n" +
                "\n" +
                "<p><b>نوبت هر بازیکن:</b></p>\n" +
                "\n" +
                "<p>هر بازیکنی که نوبتش میشه اسمش روی صفحه میاد، گوشی موبایل رو بدید به کسی که اسمش اومده، فقط و فقط خودش نگاه کنه به صفحه و کلید \"من فلانی هستم\" رو بزنه، حالا یا یک مکان میبینه یا میفهمه که جاسوس خودشه بعد دکمه \"فهمیدم\" رو میزنه و گوشی رو تحویل نفر بعدی میده.</p>\n" +
                "\n" +
                "<p>وقتی همه بازیکن ها مکان منتخب رو دیدند بازی شروع میشه، شما به اندازه زمانی که قبلا انتخاب کردید وقت دارید تا بازی رو تموم کنید. برای اینکار نفر اول باید یک سوال از هرکی که دوست داره بپرسه و طرف می تونه هرجوری که دوست داره جواب بده.</p>\n" +
                "\n" +
                "<p>نکته: وقتی کسی از شما سوال میپرسه و شما جواب میدید، شما نمیتونید تو همون همون نوبت ازش سوال بپرسید. چونکه همینطور از همدیگه سوال میپرسید و بازی تبدیل میشه به یک مرغ دارم روزی فلان قدر تخم میزاره.</p>\n" +
                "\n" +
                "<p>برای اینکه تا حدی بدونید چطوری باید سوال بپرسید ما یک سناریو برای شما می گیم:</p>\n" +
                "\n" +
                "<p>بازی شروع شده و مکان انتخابی بیمارستانه، مهدی از ایلیاد می خواد سوال کنه :</p>\n" +
                "\n" +
                "<p>مهدی: \"ایلیاد از این مکان تو ایران داریم؟\" </p>\n" +
                "\n" +
                "<p>ایلیاد: \"آره داریم.\"ایلیاد می تونه بگه حتی نمی دونم، ولی کافیه در جواب همچین سوالی بگید: نمیدونم، بی برو برگرد همه میگن جاسوس شمایید</p>\n" +
                "\n" +
                "<p>ایلیاد: \"شهریار با لباس خاصی باید رفت اونجا ؟\" </p>\n" +
                "\n" +
                "<p>شهریار: \"میشه گفت، آره یا نه\" </p>\n" +
                "\n" +
                "<p>شهریار: \"بهراد، اونجا بیشتر چه کارایی انجام میشه؟؟\"</p>\n" +
                "\n" +
                "<p>بهراد : \"نمیدونم\" ( بهترین جواب به اینجور سوال های تابلو همینه، بگید نمیدونم راحت کنید خودتون. کلا سوالی که بخواد خیلی اطلاعات به جاسوس بده، درست نیست)</p>\n" +
                "\n" +
                "<p>از همین دسته سوالات بپرسید، تا اینکه جاسوس رو پیدا کنید. یا اینکه جاسوس مکان منتخب رو حدس بزنه.</p>\n" +
                "\n" +
                "<p>نکته : سعی کنید در طول بازی از همه سوال کنید.</p>\n" +
                "\n" +
                "<p>نکته : همه باید حواسشون به سوال و جواب ها باشه، اگر کسی حواسش نبود سعی کنید تکرار نکنید، تکرار سوال و جواب می تونه به جاسوس کمک کنه.</p>\n" +
                "\n" +
                "<p>نکته: سوال های تابلو نپرسید، مثلا برای مثال بیمارستان نپرسید، اونجا مریض هارو درمان می کنند؟</p><br/>\n" +
                "\n" +
                "<p><b>پایان بازی:</b></p>\n" +
                "\n" +
                "<p>وقتی زمان تموم شد، یعنی بازی تمومه و باید جاسوس رو پیدا کنید، خیلی ساده با یک رای گیری (به نوبت و به ازای همه شرکت کننده ها) رای میدید به نفری که بهش شک دارید. اگر جاسوس رو درست انتخاب کردید که شما برنده اید، اگر نه جاسوس برندست.</p>\n" +
                "\n" +
                "<p>جاسوس هر کجا بازی مکان رو فهمید می تونه اعلام کنه و حدس بزنه.</p>\n" +
                "\n" +
                "<p>نکته : اگر همه جاسوس شده باشید، اولین کسی که حدس بزنه همه جاسوس هستند برنده است.</p><br/>\n" +
                "\n" +
                "<p>لذت ببرید ...." +
                "" +
                "</p>\n");
    }

    private void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

//    private void playVideo() {
//        try {
//            //set the media controller in the VideoView
//            mVideoView.setMediaController(null);
//
//            //set the uri of the video to be played
//            mVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.help));
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        mVideoView.requestFocus();
//        //we also set an setOnPreparedListener in order to know when the video file is ready for playback
//        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//
//            public void onPrepared(MediaPlayer mediaPlayer) {
//                // close the progress bar and play the video
//                //if we have a position on savedInstanceState, the video playback should start from here
//                mVideoView.seekTo(position);
//                if (position == 0) {
//                    mVideoView.start();
//                } else {
//                    //if we come from a resumed activity, video playback will be paused
//                    mVideoView.pause();
//                }
//            }
//        });
//    }

    private void playVideo() {
        mVideoView.setCornerRadius(104f);
        mMediaPlayer = new MediaPlayer();
        AssetFileDescriptor afd = null;
        try {
            afd = getAssets().openFd("help.mp4");
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mMediaPlayer.start();
                }
            });
            mVideoView.setMediaPlayer(mMediaPlayer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void stopMusic() {
        stopService(new Intent(this, MusicService.class));
    }

    public void setHtmlText(TextView tv, String htmlStr) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(htmlStr, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv.setText(Html.fromHtml(htmlStr));
        }
    }

//    AssetFileDescriptor afd = getAssets().openFd("help.mp4");
//    mMediaPlayer = new MediaPlayer();
//            mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
//            mMediaPlayer.setSurface(s);
//            mMediaPlayer.prepareAsync();
//
//            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//        @Override
//        public void onPrepared(MediaPlayer mediaPlayer) {
//            mediaPlayer.start();
//        }
//    });

}
