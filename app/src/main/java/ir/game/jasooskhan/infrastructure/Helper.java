package ir.game.jasooskhan.infrastructure;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.io.IOException;
import java.util.Random;

import ir.game.jasooskhan.application.Constant;
import ir.game.jasooskhan.customview.MyProgressDialog;

public class Helper {

    private static MyProgressDialog progressDialog;

    public static class progressBar {

        public static void showDialog(Context activity) {

            progressDialog = new MyProgressDialog(activity);
            progressDialog.show();
        }

        public static void hideDialog() {

            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    public static void alert(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static int getRandomIndex(int max) {
        Random random = new Random();
        return random.nextInt(max + 1);
    }

    public static void playClickSound(Context context) {
//        final MediaPlayer mp = MediaPlayer.create(context, R.raw.buttonclick);
//        mp.start();
    }

    public static void shakeView(View view) {
        YoYo.with(Techniques.Tada)
                .duration(500)
                .delay(0)
                .repeat(0)
                .playOn(view);
    }

    public static void openBazarPage(Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("bazaar://details?id=" + Constant.PACKAGE_NAME));
        intent.setPackage("com.farsitel.bazaar");
        context.startActivity(intent);
    }

    public static boolean isInternetConnected(Context context) {
        if (isNetworkConnected(context)) {
            Runtime runtime = Runtime.getRuntime();
            try {
                Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8" +
                        "");
                int exitValue = ipProcess.waitFor();
                return (exitValue == 0);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return false;
        } else {
            return false;
        }
    }

    private static boolean isNetworkConnected(Context context) {
        try {
            ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
                    || conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                return true;

            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static void animateTransition(Context context) {
        Animatoo.animateFade(context);
    }

}
