package ir.game.jasooskhan.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import ir.game.jasooskhan.R;

public class AddLocationDialogFragment extends DialogFragment implements View.OnClickListener {

    Button mBTN_add;
    Button mBTN_cancel;
    EditText mET_locationName;
    public OnLocationPlayer onLocationPlayer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        View rootView = inflater.inflate(R.layout.fragment_dialog_add_location, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mBTN_add = rootView.findViewById(R.id.fragment_dialog_add_location_btn_add);
        mBTN_cancel = rootView.findViewById(R.id.fragment_dialog_add_location_btn_cancel);
        mET_locationName = rootView.findViewById(R.id.fragment_dialog_add_location_et_name);

        mBTN_add.setOnClickListener(this);
        mBTN_cancel.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == mBTN_add.getId()) {
            if (!mET_locationName.getText().toString().isEmpty()) {
                onLocationPlayer.addNewLocation(mET_locationName.getText().toString());
            } else {

            }
        } else if (id == mBTN_cancel.getId()) {
            dismiss();
        }
    }

    public interface OnLocationPlayer {
        void addNewLocation(String name);
    }

    public void setOnAddLocationListener(OnLocationPlayer IOnAddPlayer) {
        this.onLocationPlayer = IOnAddPlayer;
    }

}
