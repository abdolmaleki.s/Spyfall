package ir.game.jasooskhan.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import ir.game.jasooskhan.R;

public class PlayerStateDialogFragment extends DialogFragment implements View.OnClickListener {

    Button mBTN_gotIt;
    TextView mTV_locationNameLabel;
    TextView mTV_locationName;
    TextView mTV_spyLabel;
    ImageView mIMG_spy;
    public OnButtonClickListener mOnButtonClickListener;

    public final static String TYPE_SPY = "spy";
    public final static String TYPE_LOCATION = "location";
    public final static String KEY_TYPE = "key.type";
    public final static String KEY_LOCATION = "key.location";
    private String mType;
    private String mLocation;


    public static PlayerStateDialogFragment newInstance(String Type, String location) {
        PlayerStateDialogFragment fragment = new PlayerStateDialogFragment();
        Bundle args = new Bundle();
        args.putString(KEY_TYPE, Type);
        args.putString(KEY_LOCATION, location);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        View rootView = inflater.inflate(R.layout.fragment_dialog_player_state, container, false);
        loadDate();
        initView(rootView);
        return rootView;
    }

    private void loadDate() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mType = bundle.getString(KEY_TYPE);
            mLocation = bundle.getString(KEY_LOCATION, null);
        }
    }

    private void initView(View rootView) {
        mBTN_gotIt = rootView.findViewById(R.id.fragment_dialog_player_state_btn_got_it);
        mTV_locationNameLabel = rootView.findViewById(R.id.fragment_dialog_player_state_label_location_name);
        mTV_spyLabel = rootView.findViewById(R.id.fragment_dialog_player_state_tv_spy_label);
        mTV_locationName = rootView.findViewById(R.id.fragment_dialog_player_state_tv_location_text);
        mIMG_spy = rootView.findViewById(R.id.fragment_dialog_player_state_img_spy);
        mBTN_gotIt.setOnClickListener(this);

        if (mType.equals(TYPE_SPY)) {
            mTV_locationNameLabel.setVisibility(View.GONE);
            mTV_locationName.setVisibility(View.GONE);
            mIMG_spy.setVisibility(View.VISIBLE);
            mTV_spyLabel.setVisibility(View.VISIBLE);

        } else {
            mTV_locationNameLabel.setVisibility(View.VISIBLE);
            mTV_locationName.setVisibility(View.VISIBLE);
            mIMG_spy.setVisibility(View.GONE);
            mTV_spyLabel.setVisibility(View.GONE);
            mTV_locationName.setText("\"" + mLocation + "\"");
        }


    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == mBTN_gotIt.getId()) {
            mOnButtonClickListener.onClick();
            dismiss();
        }
    }

    public interface OnButtonClickListener {
        void onClick();
    }

    public void setOnAddPlayerListener(OnButtonClickListener OnButtonClickListener) {
        this.mOnButtonClickListener = OnButtonClickListener;
    }

}
