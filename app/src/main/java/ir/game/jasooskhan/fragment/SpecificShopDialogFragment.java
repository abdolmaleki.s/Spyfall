package ir.game.jasooskhan.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.adapter.ShopLocationAdapter;
import ir.game.jasooskhan.model.LocationModel;
import ir.game.jasooskhan.model.LocationPackageModel;

public class SpecificShopDialogFragment extends DialogFragment implements View.OnClickListener {

    private LocationPackageModel mModel;
    private RatingBar mRatingBar;
    private TextView mTV_price;
    private TextView mTV_Description;
    private RecyclerView mListLocations;
    private Context mContext;
    private LinearLayout mBTN_purchase;
    private OnPurchaseClickListener mOnPurchaseClickListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        View rootView = inflater.inflate(R.layout.fragment_specific_shop, container, false);
        initView(rootView);
        fillData();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
    }

    private void initView(View rootView) {
        mRatingBar = rootView.findViewById(R.id.ic_specific_shop_ratingbar);
        mTV_price = rootView.findViewById(R.id.fragment_specific_shop_tv_price);
        mListLocations = rootView.findViewById(R.id.fragment_specific_shop_list_locations);
        mBTN_purchase = rootView.findViewById(R.id.fragment_specific_shop_btn_purchase);
        mTV_Description = rootView.findViewById(R.id.fragment_specific_shop_tv_package_description);
        rootView.findViewById(R.id.fragment_specific_shop_btn_exit).setOnClickListener(this);
        mBTN_purchase.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.fragment_specific_shop_btn_exit) {
            dismiss();
        } else if (id == mBTN_purchase.getId()) {
            mOnPurchaseClickListener.onItemClick(mModel);
        }
    }

    public void showSpecificShopPackage(AppCompatActivity activity, LocationPackageModel model, OnPurchaseClickListener onPurchaseClickListener) {
        mModel = model;
        mContext = activity;
        mOnPurchaseClickListener = onPurchaseClickListener;
        this.show(activity.getSupportFragmentManager(), "SpecificShopDialogFragment");

    }

    private void fillData() {

        mRatingBar.setRating(mModel.getDifficulty());
        //mTV_price.setText(mModel.getPrice() + " تومان");
        if (mModel.isEditable()) {
            mListLocations.setVisibility(View.GONE);
            mTV_Description.setVisibility(View.VISIBLE);
            mTV_Description.setText("با خرید این پکیج شما صاحب یک بسته ی جدید میشید که میتونید مکان های اون رو به دلخواه خودتون وارد کنید و تغییر بدید");
        } else {
            mListLocations.setVisibility(View.VISIBLE);
            mTV_Description.setVisibility(View.GONE);
            initAdapter();
        }
    }

    private void initAdapter() {
        List<String> locationNames = new ArrayList<>();
        for (LocationModel location : mModel.locations) {
            locationNames.add(location.getName());
        }

        ShopLocationAdapter locationAdapter = new ShopLocationAdapter(locationNames);
        mListLocations.setLayoutManager(new GridLayoutManager(mContext, 2));
        mListLocations.setAdapter(locationAdapter);
    }

    public interface OnPurchaseClickListener {
        void onItemClick(LocationPackageModel model);
    }

}
