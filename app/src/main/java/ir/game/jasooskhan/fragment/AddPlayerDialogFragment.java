package ir.game.jasooskhan.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import ir.game.jasooskhan.R;

public class AddPlayerDialogFragment extends DialogFragment implements View.OnClickListener {

    Button mBTN_add;
    Button mBTN_cancel;
    EditText mET_playerName;
    public OnAddPlayer IOnAddPlayer;
    private String defaultNameValue = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_transparent);
        }

        View rootView = inflater.inflate(R.layout.fragment_dialog_add_player, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mBTN_add = rootView.findViewById(R.id.fragment_dialog_add_player_btn_add);
        mBTN_cancel = rootView.findViewById(R.id.fragment_dialog_add_player_btn_cancel);
        mET_playerName = rootView.findViewById(R.id.fragment_dialog_add_player_et_name);

        mBTN_add.setOnClickListener(this);
        mBTN_cancel.setOnClickListener(this);
        mET_playerName.setText(defaultNameValue);
        if(defaultNameValue!=null && !defaultNameValue.isEmpty()){
            mBTN_add.setText("ویرایش");
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == mBTN_add.getId()) {
            if (!mET_playerName.getText().toString().isEmpty()) {
                IOnAddPlayer.addPlayer(mET_playerName.getText().toString());
            } else {

            }
        } else if (id == mBTN_cancel.getId()) {
            dismiss();
        }
    }

    public interface OnAddPlayer {
        void addPlayer(String name);
    }

    public void setOnAddPlayerListener(OnAddPlayer IOnAddPlayer) {
        this.IOnAddPlayer = IOnAddPlayer;
    }

    public void show(AppCompatActivity activity, String defualtValue) {

        this.show(activity.getSupportFragmentManager(), this.getClass().getName());

        if (defualtValue != null) {
            defaultNameValue = defualtValue;
        }


    }

}
