package ir.game.jasooskhan.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.model.LocationPackageModel;

public class ShopPackageAdapter extends RecyclerView.Adapter<ShopPackageAdapter.MyViewHolder> {

    private List<LocationPackageModel> mPackages;
    private OnItemClickListener onItemClickListener;

    public ShopPackageAdapter(List<LocationPackageModel> locations, OnItemClickListener onItemClickListener) {
        this.mPackages = locations;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_package_shop, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, final int position) {
        String title = mPackages.get(position).getTitle();
        viewHolder.packageName.setText((title.length() < 20) ? title : (title.substring(0, 15) + "..."));
    }

    @Override
    public int getItemCount() {
        return mPackages.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView packageName;
        ImageView picture;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            packageName = itemView.findViewById(R.id.item_list_package_shop_tv_name);
            picture = itemView.findViewById(R.id.item_list_package_shop_iv_picture);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(mPackages.get(getLayoutPosition()));
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(LocationPackageModel model);
    }


}
