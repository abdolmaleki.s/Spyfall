package ir.game.jasooskhan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import github.nisrulz.recyclerviewhelper.RVHAdapter;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.activity.SettingActivity;
import ir.game.jasooskhan.model.DB;
import ir.game.jasooskhan.viewmodel.PlayerViewModel;

public class PlayerAdapter extends RecyclerView.Adapter<PlayerAdapter.MyViewHolder> implements RVHAdapter {

    private List<PlayerViewModel> players;
    private Context mContext;
    private OnClickListener mOnClickListener;


    public PlayerAdapter(List<PlayerViewModel> players, Context context, OnClickListener onClickListener) {
        this.players = players;
        mContext = context;
        mOnClickListener = onClickListener;
    }

    @androidx.annotation.NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@androidx.annotation.NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rcv_player, parent, false);
        return new PlayerAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@androidx.annotation.NonNull MyViewHolder holder, int position) {
        final PlayerViewModel currentModel = players.get(position);
        holder.player.setText(currentModel.name);
        switch (position % 3) {
            case 0:
                holder.rightIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_setting_player_name_right_icon_red));
                break;
            case 1:
                holder.rightIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_setting_player_name_right_icon_blue));
                break;
            case 2:
                holder.rightIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_setting_player_name_right_icon_green));
                break;
        }

        holder.editIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnClickListener.onEdit(currentModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    @Override
    public void onItemDismiss(int position, int direction) {
        DB.Player.delete(players.get(position).id);
        players.remove(position);
        notifyItemRemoved(position);
        ((SettingActivity) mContext).initSpyCounter();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        swap(fromPosition, toPosition);
        return false;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView player;
        ImageView rightIcon;
        ImageView editIcon;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            player = itemView.findViewById(R.id.item_rcv_player_name);
            rightIcon = itemView.findViewById(R.id.item_rcv_player_img_right_icon);
            editIcon = itemView.findViewById(R.id.item_rcv_player_img_edit);
        }
    }

    private void swap(int firstPosition, int secondPosition) {
        Collections.swap(players, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    public List<PlayerViewModel> getDataset() {
        return players;
    }

    public void addPlayer(PlayerViewModel playerViewModel) {
        players.add(playerViewModel);
        notifyDataSetChanged();
    }

    public interface OnClickListener {
        void onEdit(PlayerViewModel model);
    }
}
