package ir.game.jasooskhan.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.model.DB;
import ir.game.jasooskhan.model.LocationPackageModel;

public class SettingPackageAdapter extends RecyclerView.Adapter<SettingPackageAdapter.MyViewHolder> {

    private List<LocationPackageModel> mPackages;

    public SettingPackageAdapter(List<LocationPackageModel> locations) {
        this.mPackages = locations;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_package_setting, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, final int position) {
        String title = mPackages.get(position).getTitle();
        viewHolder.packageName.setText((title.length() < 15) ? title : (title.substring(0, 15) + "..."));
        viewHolder.removeLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DB.LocationPackage.setSelected(mPackages.get(position).getId(), false);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPackages.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView packageName;
        ImageView removeLocation;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            packageName = itemView.findViewById(R.id.item_list_package_setting_tv_name);
            removeLocation = itemView.findViewById(R.id.item_list_package_setting_iv_remove);
        }
    }


}
