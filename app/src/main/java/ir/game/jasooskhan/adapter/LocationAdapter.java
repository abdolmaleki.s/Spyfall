package ir.game.jasooskhan.adapter;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ir.game.jasooskhan.R;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.MyViewHolder> {

    private List<String> mLocations;

    public LocationAdapter(List<String> locations) {
        this.mLocations = locations;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_grid_location, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, final int position) {
        String location = mLocations.get(position);
        if (location.split(" ").length == 1 && location.length() > 6) {
            viewHolder.location.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        }
        if (location.length() > 13) {
            location = location.substring(0, 13) + "...";
        }
        viewHolder.location.setText(location);

    }

    @Override
    public int getItemCount() {
        return mLocations.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView location;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            location = itemView.findViewById(R.id.item_grid_location_name);
        }
    }


}
