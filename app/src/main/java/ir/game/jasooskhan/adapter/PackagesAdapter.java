package ir.game.jasooskhan.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.fragment.AddLocationDialogFragment;
import ir.game.jasooskhan.model.DB;
import ir.game.jasooskhan.model.LocationModel;
import ir.game.jasooskhan.model.LocationPackageModel;

public class PackagesAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<LocationPackageModel> packageModels;

    public PackagesAdapter(Context context, List<LocationPackageModel> packageModels) {
        this.context = context;
        this.packageModels = packageModels;
        DB.init();
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.packageModels.get(listPosition).locations.get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        LocationPackageModel parentModel = packageModels.get(listPosition);
        final String expandedListText = parentModel.locations.get(expandedListPosition).getName();
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_list_package_child, null);
        }
        convertView.setBackgroundColor(ContextCompat.getColor(context, R.color.jegari_text));

        TextView detailTitle = convertView.findViewById(R.id.item_rcv_package_detail_title);
        if (parentModel.isEditable()) {
            TextView deleteText = convertView.findViewById(R.id.item_rcv_package_detail_tv_delete_item);
            deleteText.setVisibility(View.VISIBLE);
            deleteText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        DB.Location.delete(parentModel.getLocations().get(expandedListPosition).getId());
                        PackagesAdapter.this.notifyDataSetChanged();
                    } catch (Exception e) {

                    }
                }
            });

        }

        detailTitle.setText(expandedListText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.packageModels.get(listPosition).locations.size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.packageModels.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.packageModels.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        final LocationPackageModel currentModel = ((LocationPackageModel) getGroup(listPosition));
        String listTitle = currentModel.getTitle();

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_list_package_group, null);
        }

        TextView packageTitle = convertView.findViewById(R.id.item_list_package_title);
        ImageView imgAdd = convertView.findViewById(R.id.item_list_package_switch_img_add_location);
        ImageView imgArrow = convertView.findViewById(R.id.item_list_package_iv_arrow);
        final ImageView imgCheck = convertView.findViewById(R.id.item_list_package_iv_check);
        FrameLayout panelCheck = convertView.findViewById(R.id.item_list_package_panel_check);
        ConstraintLayout panelBackOuter = convertView.findViewById(R.id.item_list_package_panel_back_outer);
        ConstraintLayout panelBackInner = convertView.findViewById(R.id.item_list_package_panel_back_inner);

        packageTitle.setText(listTitle);
        imgCheck.setVisibility(currentModel.isSelected() ? View.VISIBLE : View.GONE);
        imgArrow.setRotation(isExpanded ? 180 : 0);


        // Custom Package //////////////////////////////////////////////////////////////////
        if (currentModel.isEditable()) {
            imgAdd.setVisibility(View.VISIBLE);
            panelBackOuter.setBackgroundColor(ContextCompat.getColor(context, R.color.my_package_back_orange));
            panelBackInner.setBackgroundColor(ContextCompat.getColor(context, R.color.my_package_back_yellow));
            panelCheck.setBackgroundColor(ContextCompat.getColor(context, R.color.my_package_back_orange));
            packageTitle.setTextColor(ContextCompat.getColor(context, R.color.white));

        } else {
            imgAdd.setVisibility(View.GONE);
            panelBackOuter.setBackgroundColor(ContextCompat.getColor(context, R.color.package_back_shiri));
            panelBackInner.setBackgroundColor(ContextCompat.getColor(context, R.color.package_back_keremi));
            panelCheck.setBackgroundColor(ContextCompat.getColor(context, R.color.packagelist_group_check_back));
            packageTitle.setTextColor(ContextCompat.getColor(context, R.color.packagelist_group_title));
        }
        ////////////////////////////////////////////////////////////////////


        panelCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imgCheck.getVisibility() == View.VISIBLE) {
                    DB.LocationPackage.setSelected(currentModel.getId(), false);
                    imgCheck.setVisibility(View.GONE);
                } else {
                    if (currentModel.locations != null && currentModel.locations.size() > 0) {
                        DB.LocationPackage.setSelected(currentModel.getId(), true);
                        imgCheck.setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(context, "هیچ مکانی در این گروه ثبت نشده است.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AddLocationDialogFragment dialogFragment = new AddLocationDialogFragment();
                dialogFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), AddLocationDialogFragment.class.getName());
                dialogFragment.setOnAddLocationListener(new AddLocationDialogFragment.OnLocationPlayer() {
                    @Override
                    public void addNewLocation(String name) {
                        LocationModel locationModel = new LocationModel(name);
                        DB.LocationPackage.addLocation(currentModel.getId(), locationModel);
                        notifyDataSetChanged();
                        dialogFragment.dismiss();
                    }
                });
            }
        });
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}

