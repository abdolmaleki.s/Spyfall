package ir.game.jasooskhan.service;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import java.util.Objects;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import ir.game.jasooskhan.R;
import ir.game.jasooskhan.activity.TimerActivity;
import ir.game.jasooskhan.application.Constant;
import ir.game.jasooskhan.setting.Session;

public class TimerService extends Service {

    public static final String COMMAND_PAUSE = "COMMAND_PAUSE";
    public static final String COMMAND_RESUME = "COMMAND_RESUME";

    private final static String TAG = "BroadcastService";
    private Session mSession;
    private MediaPlayer mPlayer;
    public static final String COUNTDOWN_BR = " ir.game.spyfall.countdown_br";
    Intent bi = new Intent(COUNTDOWN_BR);
    CountDownTimer cdt = null;
    private long mGameRemainTime;


    @Override
    public void onCreate() {
        super.onCreate();
        mSession = Session.getInstance(getApplicationContext());
        computeRemainingTime();
        initAudioPlayer();

    }

    private void computeRemainingTime() {
        int durationInMinut = mSession.getGameDuration();
        mGameRemainTime = 60 * durationInMinut * 1000; //in milisecond;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mSession.setIsGameRunning(true);
        setNotification();
        String command = Objects.requireNonNull(intent.getExtras()).getString(Constant.Key.SERVICE_COMMAND);
        if (command != null) {
            if (command.equals(COMMAND_PAUSE)) {
                pauseTimer();
            } else if (command.equals(COMMAND_RESUME)) {
                startTimer();
            }
        }
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        cdt.cancel();
        mPlayer.stop();
        mPlayer.release();
        mSession.setIsGameRunning(false);
        super.onDestroy();

    }

    private void startTimer() {
        mSession.setIsAGameFinished(false);
        cdt = new CountDownTimer(mGameRemainTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                bi.putExtra(Constant.Key.TIME_REMAIN, millisUntilFinished);
                sendBroadcast(bi);
                if (millisUntilFinished <= 61000 && millisUntilFinished >= 60000) {
                    mPlayer.start();
                }
                mGameRemainTime = millisUntilFinished;
            }

            @Override
            public void onFinish() {
                Session.getInstance(getApplicationContext()).setIsAGameFinished(true);
                playFinishAlarm();
            }
        };
        cdt.start();
    }

    private void playFinishAlarm() {
        mPlayer = MediaPlayer.create(this, R.raw.finish);
        mPlayer.start();
    }

    private void pauseTimer() {
        cdt.cancel();
    }

    private void initAudioPlayer() {
        mPlayer = MediaPlayer.create(this, R.raw.lastminut);
    }


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void setNotification() {
        PendingIntent contentIntent;
        contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, TimerActivity.class), 0);

        String channelId = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotificationChannel("my_service", "My Background Service");
        }

        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_timer_spy)
                        .setContentTitle("بازی در حال اجرا")
                        .setOngoing(true)
                        .setAutoCancel(false);
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(3650, mBuilder.build());

        Notification notification = mBuilder.build();
        startForeground(3650, notification); //NOTIFICATION_ID is a random integer value which has to be unique in an app
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(String channelId, String channelName) {
        NotificationChannel chan = new NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        service.createNotificationChannel(chan);
        return channelId;
    }

}