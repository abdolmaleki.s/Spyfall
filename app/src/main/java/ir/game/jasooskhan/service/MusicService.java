package ir.game.jasooskhan.service;


import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

import java.util.List;
import java.util.Objects;

import ir.game.jasooskhan.R;
import ir.game.jasooskhan.application.Constant;

public class MusicService extends Service {

    public static final String PLAY_MUSIC = "PLAY_MUSIC";
    public static final String STOP_MUSIC = "STOP_MUSIC";
    private MediaPlayer mPlayer;

    @Override
    public void onCreate() {
        super.onCreate();
        initAudioPlayer();

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            String command = Objects.requireNonNull(intent.getExtras()).getString(Constant.Key.MUSIC_COMMAND);
            if (command != null) {
                if (command.equals(PLAY_MUSIC)) {
                    if (!mPlayer.isPlaying()) {
                        initAudioPlayer();
                        mPlayer.start();
                    }
                } else if (command.equals(STOP_MUSIC)) {
                    checkNeedPause();
                }
            }
        } catch (Exception e) {

        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        mPlayer.stop();
        mPlayer.release();
        super.onDestroy();
    }

    private void initAudioPlayer() {
        mPlayer = MediaPlayer.create(this, R.raw.backgroundmusic);
        mPlayer.setLooping(true);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void checkNeedPause() {
        if (!isAppOnForeground(this, "ir.game.spyfall")) {
            mPlayer.stop();
        }
    }

    private boolean isAppOnForeground(Context context, String appPackageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = appPackageName;
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                //                Log.e("app",appPackageName);
                return true;
            }
        }
        return false;
    }
}